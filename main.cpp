/*
*  Copyright (c) 2014, Oculus VR, Inc.
*  All rights reserved.
*
*  This source code is licensed under the BSD-style license found in the
*  LICENSE file in the root directory of this source tree. An additional grant
*  of patent rights can be found in the PATENTS file in the same directory.
*
*/

// ----------------------------------------------------------------------
// RakNet version 1.0
// Filename ChatExample.cpp
// Very basic chat engine example
// ----------------------------------------------------------------------


#include "header.h"


#if LIBCAT_SECURITY==1
#include "SecureHandshake.h" // Include header for secure handshake
#endif

#if defined(_CONSOLE_2)
#include "Console2SampleIncludes.h"
#endif

// We copy this from Multiplayer.cpp to keep things all in one file for this example
unsigned char GetPacketIdentifier(RakNet::Packet *p);

#ifdef _CONSOLE_2
_CONSOLE_2_SetSystemProcessParams
#endif

int main(int argc, char* argv[])
{

	std::vector<std::string> argVector;

	std::cout << "argc = " << argc << std::endl;

	if (argc != 1)
	{
		for (int i = 0; i < argc; i++)
		{
			std::cout << "argv[" << i << "] = " << argv[i] << std::endl;
			argVector.push_back(argv[i]);
		}

		if (argVector.at(1) == "0")
		{
			startServer(argVector);
		}

		else
		{
			startClient(argVector);
		}
	}

	else
	{
		std::cout << "No Arguments\n";
	}


	std::cout << std::endl;
	

	return 0;
}