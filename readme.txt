Group Members:
Daniel Covert
Gil Popson

Repository:
https://CovertCoding@bitbucket.org/CovertCoding/egp405_a1_02_covert_popson.git

Description:
The application is a simple console game of connect 4.

How To Play:
-After building the project simply double click 'connect4.bat' to open up and run both the client and server.
-Alternatively, you can singly run either the 'connect4ClientOnly.bat' or 'connect4ServerOnly.bat' for their respective client server programs.


-The server begins the game by entering either s or S into the console.
Once the game has started the server make the first move.
Each player(when it is their turn) will type in any number between 1-7 to drop their piece
the numbers represent the columbs in the game, the cooresponding colum number combo will
be listed in the GUI in game.
Server chips will be represented by 'X', client chips will be represented by 'O'