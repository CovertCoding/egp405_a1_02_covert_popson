#include "header.h"

#if LIBCAT_SECURITY==1
#include "SecureHandshake.h" // Include header for secure handshake
#endif

#if defined(_CONSOLE_2)
#include "Console2SampleIncludes.h"
#endif

// We copy this from Multiplayer.cpp to keep things all in one file for this example
unsigned char clientGetPacketIdentifier(RakNet::Packet *p);

#ifdef _CONSOLE_2
_CONSOLE_2_SetSystemProcessParams
#endif

void drawClientGrid2();
void drawClientGrid(symbols::Board* theBoard);
int getChipPositionC(int col, symbols::Board theBoard);
bool checkInputC(char theInput[2048]);
bool checkBoardFilledC(symbols::Board theBoard);
bool isColumnFilledC(int col, symbols::Board theBoard);
void updateBoardC(symbols::Board theBoard);
void setRowColPosC(symbols::Board& theBoard);
bool checkConnectFourOC(symbols::Board& theBoard);

void startClient(std::vector<std::string> vec)
{
#pragma region CLIENT
#pragma region CLIENT_START_UP


	std::vector<std::string> argVector = vec;

//	RakNet::RakNetStatistics *rss;
	// Pointers to the interfaces of our server and client.
	// Note we can easily have both in the same program
	RakNet::RakPeerInterface *client = RakNet::RakPeerInterface::GetInstance();
	//client->InitializeSecurity(0,0,0,0);
	//RakNet::PacketLogger packetLogger;
	//client->AttachPlugin(&packetLogger);

	// Holds packets
	RakNet::Packet* p;

	// GetPacketIdentifier returns this
	unsigned char packetIdentifier;

	// Just so we can remember where the packet came from
	bool isServer;

	// Record the first client that connects to us so we can pass it to the ping function
	RakNet::SystemAddress clientID = RakNet::UNASSIGNED_SYSTEM_ADDRESS;

	// Crude interface

	// Holds user data
	char ip[64], serverPort[30], clientPort[30];

	// A client
	isServer = false;

	//printf("This is a sample implementation of a text based chat client.\n");
	//printf("Connect to the project 'Chat Example Server'.\n");
	//printf("Difficulty: Beginner\n\n");

	// Get our input
	std::string portName = "Client now listening on port(" + argVector.at(3) + ")";
	puts(portName.c_str());

	/*
	Taking this away cause seems unnecessary for now?
	*/
	//Gets(clientPort, sizeof(clientPort));
	//if (clientPort[0] == 0)
	//strcpy(clientPort, "0");

	for (unsigned int i = 0; i < argVector.at(2).size(); i++)
	{
		ip[i] = argVector.at(2).at(i);
	}

	std::string ipName = "Connecting to IP(" + argVector.at(2) + ")";
	puts(ipName.c_str());
	//Gets(ip, sizeof(ip));
	client->AllowConnectionResponseIPMigration(false);
	//if (ipName.at(0) == '0')
		//strcpy(ip, "127.0.0.1");


	std::string portConnectName = "Now Connecting to port(" + argVector.at(4) + ")";
	puts(portConnectName.c_str());


	//Gets(serverPort, sizeof(serverPort));

	for (unsigned int i = 0; i < argVector.at(3).size(); i++)
	{
		clientPort[i] = argVector.at(3).at(i);
	}


	for (unsigned int i = 0; i < argVector.at(4).size(); i++)
	{
		serverPort[i] = argVector.at(4).at(i);
	}
	//if (serverPort[0] == 0)
		//strcpy(serverPort, "1234");

	// Connecting the client is very simple.  0 means we don't care about
	// a connectionValidationInteger, and false for low priority threads
	RakNet::SocketDescriptor socketDescriptor(atoi(clientPort), 0);
	socketDescriptor.socketFamily = AF_INET;
	client->Startup(8, &socketDescriptor, 1);
	client->SetOccasionalPing(true);


#if LIBCAT_SECURITY==1
	char public_key[cat::EasyHandshake::PUBLIC_KEY_BYTES];
	FILE *fp = fopen("publicKey.dat", "rb");
	fread(public_key, sizeof(public_key), 1, fp);
	fclose(fp);
#endif

#if LIBCAT_SECURITY==1
	RakNet::PublicKey pk;
	pk.remoteServerPublicKey = public_key;
	pk.publicKeyMode = RakNet::PKM_USE_KNOWN_PUBLIC_KEY;
	bool b = client->Connect(ip, atoi(serverPort), "Rumpelstiltskin", (int)strlen("Rumpelstiltskin"), &pk) == RakNet::CONNECTION_ATTEMPT_STARTED;
#else
	RakNet::ConnectionAttemptResult car = client->Connect(ip, atoi(serverPort), "Rumpelstiltskin", (int)strlen("Rumpelstiltskin"));
	RakAssert(car == RakNet::CONNECTION_ATTEMPT_STARTED);
#endif

	printf("\nMy IP addresses:\n");
	unsigned int i;
	for (i = 0; i < client->GetNumberOfAddresses(); i++)
	{
		printf("%i. %s\n", i + 1, client->GetLocalIP(i));
	}

	printf("My GUID is %s\n", client->GetGuidFromSystemAddress(RakNet::UNASSIGNED_SYSTEM_ADDRESS).ToString());
	//puts("'quit' to quit. 'stat' to show stats. 'ping' to ping.\n'disconnect' to disconnect. 'connect' to reconnnect. Type to talk.");

	char message[2048];

#pragma endregion


	std::cout << "Welcome to 'Connect 4'!\n";
	std::cout << "When the server is ready, the game will begin\n";
	// Loop for input

	//RakNet::BitStream bitStream;
	//RakNet::MessageID useTimeStamp = ID_TIMESTAMP;
	//RakNet::Time timeStamp = RakNet::GetTime();
	//RakNet::MessageID typeID = ID_GAME_GRID_PACKET;

	symbols::Board board;
	bool gameStarted = false;
	bool myTurn = false;
	bool playerConnected = false;
	bool inputGood = false;
	bool skipInput = true;
	bool skip = true;

	board.init();
	while (1)
	{
		// This sleep keeps RakNet responsive
#ifdef _WIN32
		Sleep(30);
#else
		usleep(30 * 1000);
#endif


		if (_kbhit())
		{
			Gets(message, sizeof(message));

			if (gameStarted == true && myTurn == true)
			{
				inputGood = checkInputC(message);

				if (inputGood)
				{
					int col = atoi(message);

					if (!isColumnFilledC(col, board))
					{
						int chipPos = getChipPositionC(col, board);
						board.m_board[chipPos] = symbols::Board::CIRCLE;
						board.m_LastMovePos = chipPos;

						updateBoardC(board);
						checkConnectFourOC(board);

						client->Send((const char *)&board, sizeof(board), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
						myTurn = false;
					}
				}
			}

			else
				std::cout << "Wait your turn!\n\n";
		}

		// Get a packet from either the server or the client

		for (p = client->Receive(); p; client->DeallocatePacket(p), p = client->Receive())
		{
			// We got a packet, get the identifier with our handy function
			packetIdentifier = clientGetPacketIdentifier(p);

			// Check if this is a network message packet
			switch (packetIdentifier)
			{
			case ID_DISCONNECTION_NOTIFICATION:
				// Connection lost normally
				printf("ID_DISCONNECTION_NOTIFICATION\n");
				break;
			case ID_ALREADY_CONNECTED:
				// Connection lost normally
				printf("ID_ALREADY_CONNECTED with guid %" PRINTF_64_BIT_MODIFIER "u\n", p->guid);
				break;
			case ID_INCOMPATIBLE_PROTOCOL_VERSION:
				printf("ID_INCOMPATIBLE_PROTOCOL_VERSION\n");
				break;
			case ID_REMOTE_DISCONNECTION_NOTIFICATION: // Server telling the clients of another client disconnecting gracefully.  You can manually broadcast this in a peer to peer enviroment if you want.
				printf("ID_REMOTE_DISCONNECTION_NOTIFICATION\n");
				break;
			case ID_REMOTE_CONNECTION_LOST: // Server telling the clients of another client disconnecting forcefully.  You can manually broadcast this in a peer to peer enviroment if you want.
				printf("ID_REMOTE_CONNECTION_LOST\n");
				break;
			case ID_REMOTE_NEW_INCOMING_CONNECTION: // Server telling the clients of another client connecting.  You can manually broadcast this in a peer to peer enviroment if you want.
				printf("ID_REMOTE_NEW_INCOMING_CONNECTION\n");
				break;
			case ID_CONNECTION_BANNED: // Banned from this server
				printf("We are banned from this server.\n");
				break;
			case ID_CONNECTION_ATTEMPT_FAILED:
				printf("Connection attempt failed\n");
				break;
			case ID_NO_FREE_INCOMING_CONNECTIONS:
				// Sorry, the server is full.  I don't do anything here but
				// A real app should tell the user
				printf("ID_NO_FREE_INCOMING_CONNECTIONS\n");
				break;

			case ID_NEW_INCOMING_CONNECTION:

				//client->Send((const char *) &board, sizeof(board), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
				break;

			case ID_INVALID_PASSWORD:
				printf("ID_INVALID_PASSWORD\n");
				break;

				////////////////////////////////////////////
			case symbols::messages::ID_YOUR_TURN:

				using namespace symbols;
				Board* b;
				b = reinterpret_cast<Board*>(p->data);


				board = *b;
				//drawClientGrid(b);
				updateBoardC(board);

				if (checkConnectFourOC(board))
				{
					std::cout << "GAME OVER\n\n";
				}

				else
				{
					myTurn = true;
				}

				gameStarted = true;
				//symbols::doLogic(*b);


				break;
				/////////////////////////////////////////////

			case ID_CONNECTION_LOST:
				// Couldn't deliver a reliable packet - i.e. the other system was abnormally
				// terminated
				printf("ID_CONNECTION_LOST\n");
				break;

			case ID_CONNECTION_REQUEST_ACCEPTED:
				// This tells the client they have connected
				printf("ID_CONNECTION_REQUEST_ACCEPTED to %s with GUID %s\n", p->systemAddress.ToString(true), p->guid.ToString());
				printf("My external address is %s\n", client->GetExternalID(p->systemAddress).ToString(true));
				break;
			case ID_CONNECTED_PING:


			case ID_UNCONNECTED_PING:
				printf("Ping from %s\n", p->systemAddress.ToString(true));
				break;
			default:
				// It's a client, so just show the message
				printf("%s\n", p->data);
				break;
			}
		}
	}

	// Be nice and let the server know we quit.
	client->Shutdown(300);

	// We're done with the network
	RakNet::RakPeerInterface::DestroyInstance(client);



#pragma endregion
}

// Copied from Multiplayer.cpp
// If the first byte is ID_TIMESTAMP, then we want the 5th byte
// Otherwise we want the 1st byte
unsigned char clientGetPacketIdentifier(RakNet::Packet *p)
{
	if (p == 0)
		return 255;

	if ((unsigned char)p->data[0] == ID_TIMESTAMP)
	{
		RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
		return (unsigned char)p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
	}
	else
		return (unsigned char)p->data[0];
}

using namespace symbols;
bool checkConnectFourOC(symbols::Board& theBoard)
{
	int pos = theBoard.m_LastMovePos;


	//get row pos
	for (unsigned int i = 0; i < 6; i++)
	{
		//for first row
		if (i == 0)
		{
			//check elements
			for (unsigned int j = 0; j < 7; j++)
			{
				//found element in row
				if (pos == row0[j])
				{
					theBoard.m_RowPos = 0;
				}
			}
		}

		if (i == 1)
		{
			//check elements
			for (unsigned int j = 0; j < 7; j++)
			{
				//found element in row
				if (pos == row1[j])
				{
					theBoard.m_RowPos = 1;
				}
			}
		}

		if (i == 2)
		{
			//check elements
			for (unsigned int j = 0; j < 7; j++)
			{
				//found element in row
				if (pos == row2[j])
				{
					theBoard.m_RowPos = 2;
				}
			}
		}

		if (i == 3)
		{
			//check elements
			for (unsigned int j = 0; j < 7; j++)
			{
				//found element in row
				if (pos == row3[j])
				{
					theBoard.m_RowPos = 3;
				}
			}
		}

		if (i == 4)
		{
			//check elements
			for (unsigned int j = 0; j < 7; j++)
			{
				//found element in row
				if (pos == row4[j])
				{
					theBoard.m_RowPos = 4;
				}
			}
		}

		if (i == 5)
		{
			//check elements
			for (unsigned int j = 0; j < 7; j++)
			{
				//found element in row
				if (pos == row5[j])
				{
					theBoard.m_RowPos = 5;
				}
			}
		}
	}

	//get column pos
	for (unsigned int i = 0; i < 7; i++)
	{
		if (i == 0)
		{
			for (unsigned int j = 0; j < 6; j++)
			{
				if (pos == col0[j])
				{
					theBoard.m_ColPos = 0;
				}
			}
		}

		if (i == 1)
		{
			for (unsigned int j = 0; j < 6; j++)
			{
				if (pos == col1[j])
				{
					theBoard.m_ColPos = 1;
				}
			}
		}

		if (i == 2)
		{
			for (unsigned int j = 0; j < 6; j++)
			{
				if (pos == col2[j])
				{
					theBoard.m_ColPos = 2;
				}
			}
		}

		if (i == 3)
		{
			for (unsigned int j = 0; j < 6; j++)
			{
				if (pos == col3[j])
				{
					theBoard.m_ColPos = 3;
				}
			}
		}

		if (i == 4)
		{
			for (unsigned int j = 0; j < 6; j++)
			{
				if (pos == col4[j])
				{
					theBoard.m_ColPos = 4;
				}
			}
		}

		if (i == 5)
		{
			for (unsigned int j = 0; j < 6; j++)
			{
				if (pos == col5[j])
				{
					theBoard.m_ColPos = 5;
				}
			}
		}

		if (i == 6)
		{
			for (unsigned int j = 0; j < 6; j++)
			{
				if (pos == col6[j])
				{
					theBoard.m_ColPos = 6;
				}
			}
		}
	}



	int colPos = theBoard.m_ColPos;
	int rowPos = theBoard.m_RowPos;

	char board[6][7];

	//int holder;

	for (unsigned int i = 0; i < 6; i++)//ROW
	{
		for (unsigned int j = 0; j < 7; j++)//COL
		{
			if (i == 0)
			{
				board[i][j] = theBoard.getSlot(j);
			}

			else
			{
				board[i][j] = theBoard.getSlot((i * 7) + j);
			}
		}
	}

	int count = 0;


	for (unsigned int row = 0; row <= 2; row++)
		for (unsigned int col = 0; col <= 6; col++)
			if (board[row][col] != '-' &&
				board[row][col] == board[row + 1][col] &&
				board[row][col] == board[row + 2][col] &&
				board[row][col] == board[row + 3][col])
			{
				return true;
			}

	for (unsigned int row = 0; row <= 5; row++)
		for (unsigned int col = 0; col <= 3; col++)
			if (board[row][col] != '-' &&
				board[row][col] == board[row][col + 1] &&
				board[row][col] == board[row][col + 2] &&
				board[row][col] == board[row][col + 3])
			{
				return true;
			}


	for (unsigned int row = 0; row <= 2; row++)
		for (unsigned int col = 0; col <= 3; col++)
			if (board[row][col] != '-' &&
				board[row][col] == board[row + 1][col + 1] &&
				board[row][col] == board[row + 2][col + 2] &&
				board[row][col] == board[row + 3][col + 3])
			{
				return true;
			}

	for (unsigned int row = 3; row <= 5; row++)
		for (unsigned int col = 0; col <= 3; col++)
			if (board[row][col] != '-' &&
				board[row][col] == board[row - 1][col + 1] &&
				board[row][col] == board[row - 2][col + 2] &&
				board[row][col] == board[row - 3][col + 3])
			{
				return true;
			}


	return false;
}

void setRowColPos(symbols::Board& theBoard)
{

	//Get Row position
	for (unsigned int i = 0; i < 6; i++)
	{
		for (unsigned int j = 0; j < 7; j++)
		{
			//if (theBoard.m_LastMovePos == )
		}
	}
}


void updateBoardC(symbols::Board theBoard)
{
	system("cls");
	std::cout << "  1   2   3   4   5   6   7\n"
		<< "|-" << theBoard.getSlot(0) << "-|-" << theBoard.getSlot(1) << "-|-" << theBoard.getSlot(2) << "-|-" << theBoard.getSlot(3)
		<< "-|-" << theBoard.getSlot(4) << "-|-" << theBoard.getSlot(5) << "-|-" << theBoard.getSlot(6) << "-|\n"
		<< "|-" << theBoard.getSlot(7) << "-|-" << theBoard.getSlot(8) << "-|-" << theBoard.getSlot(9) << "-|-" << theBoard.getSlot(10)
		<< "-|-" << theBoard.getSlot(11) << "-|-" << theBoard.getSlot(12) << "-|-" << theBoard.getSlot(13) << "-|\n"
		<< "|-" << theBoard.getSlot(14) << "-|-" << theBoard.getSlot(15) << "-|-" << theBoard.getSlot(16) << "-|-" << theBoard.getSlot(17)
		<< "-|-" << theBoard.getSlot(18) << "-|-" << theBoard.getSlot(19) << "-|-" << theBoard.getSlot(20) << "-|\n"
		<< "|-" << theBoard.getSlot(21) << "-|-" << theBoard.getSlot(22) << "-|-" << theBoard.getSlot(23) << "-|-" << theBoard.getSlot(24)
		<< "-|-" << theBoard.getSlot(25) << "-|-" << theBoard.getSlot(26) << "-|-" << theBoard.getSlot(27) << "-|\n"
		<< "|-" << theBoard.getSlot(28) << "-|-" << theBoard.getSlot(29) << "-|-" << theBoard.getSlot(30) << "-|-" << theBoard.getSlot(31)
		<< "-|-" << theBoard.getSlot(32) << "-|-" << theBoard.getSlot(33) << "-|-" << theBoard.getSlot(34) << "-|\n"
		<< "|-" << theBoard.getSlot(35) << "-|-" << theBoard.getSlot(36) << "-|-" << theBoard.getSlot(37) << "-|-" << theBoard.getSlot(38)
		<< "-|-" << theBoard.getSlot(39) << "-|-" << theBoard.getSlot(40) << "-|-" << theBoard.getSlot(41) << "-|\n";

}

void drawClientGrid2()
{
	system("cls");
	std::cout << "Enter into the console which column you'd like to drop your piece into\n\n";

	std::cout << "  1   2   3   4   5   6   7\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n";
}


int getChipPositionC(int col, symbols::Board theBoard)
{
	int chipPos = 0;

	if (col == 1)
	{
		if (theBoard.isEmpty(col0[5]))
			chipPos = col0[5];

		else if (theBoard.isEmpty(col0[4]))
			chipPos = col0[4];

		else if (theBoard.isEmpty(col0[3]))
			chipPos = col0[3];

		else if (theBoard.isEmpty(col0[2]))
			chipPos = col0[2];

		else if (theBoard.isEmpty(col0[1]))
			chipPos = col0[1];

		else
			chipPos = col0[0];

		return chipPos;
	}

	else if (col == 2)
	{
		if (theBoard.isEmpty(col1[5]))
			chipPos = col1[5];

		else if (theBoard.isEmpty(col1[4]))
			chipPos = col1[4];

		else if (theBoard.isEmpty(col1[3]))
			chipPos = col1[3];

		else if (theBoard.isEmpty(col1[2]))
			chipPos = col1[2];

		else if (theBoard.isEmpty(col1[1]))
			chipPos = col1[1];

		else
			chipPos = col1[0];

		return chipPos;
	}

	else if (col == 3)
	{
		if (theBoard.isEmpty(col2[5]))
			chipPos = col2[5];

		else if (theBoard.isEmpty(col2[4]))
			chipPos = col2[4];

		else if (theBoard.isEmpty(col2[3]))
			chipPos = col2[3];

		else if (theBoard.isEmpty(col2[2]))
			chipPos = col2[2];

		else if (theBoard.isEmpty(col2[1]))
			chipPos = col2[1];

		else
			chipPos = col2[0];

		return chipPos;
	}

	else if (col == 4)
	{
		if (theBoard.isEmpty(col3[5]))
			chipPos = col3[5];

		else if (theBoard.isEmpty(col3[4]))
			chipPos = col3[4];

		else if (theBoard.isEmpty(col3[3]))
			chipPos = col3[3];

		else if (theBoard.isEmpty(col3[2]))
			chipPos = col3[2];

		else if (theBoard.isEmpty(col3[1]))
			chipPos = col3[1];

		else
			chipPos = col3[0];

		return chipPos;
	}

	else if (col == 5)
	{
		if (theBoard.isEmpty(col4[5]))
			chipPos = col4[5];

		else if (theBoard.isEmpty(col4[4]))
			chipPos = col4[4];

		else if (theBoard.isEmpty(col4[3]))
			chipPos = col4[3];

		else if (theBoard.isEmpty(col4[2]))
			chipPos = col4[2];

		else if (theBoard.isEmpty(col4[1]))
			chipPos = col4[1];

		else
			chipPos = col4[0];

		return chipPos;
	}

	else if (col == 6)
	{
		if (theBoard.isEmpty(col5[5]))
			chipPos = col5[5];

		else if (theBoard.isEmpty(col5[4]))
			chipPos = col5[4];

		else if (theBoard.isEmpty(col5[3]))
			chipPos = col5[3];

		else if (theBoard.isEmpty(col5[2]))
			chipPos = col5[2];

		else if (theBoard.isEmpty(col5[1]))
			chipPos = col5[1];

		else
			chipPos = col5[0];

		return chipPos;
	}

	else if (col == 7)
	{
		if (theBoard.isEmpty(col6[5]))
			chipPos = col6[5];

		else if (theBoard.isEmpty(col6[4]))
			chipPos = col6[4];

		else if (theBoard.isEmpty(col6[3]))
			chipPos = col6[3];

		else if (theBoard.isEmpty(col6[2]))
			chipPos = col6[2];

		else if (theBoard.isEmpty(col6[1]))
			chipPos = col6[1];

		else
			chipPos = col6[0];

		return chipPos;
	}

	return 0;
}

bool checkInputC(char theInput[2048])
{
	bool good = false;

	if (theInput[0] == '1')
		good = true;

	else if (theInput[0] == '2')
		good = true;

	else if (theInput[0] == '3')
		good = true;

	else if (theInput[0] == '4')
		good = true;

	else if (theInput[0] == '5')
		good = true;

	else if (theInput[0] == '6')
		good = true;

	else if (theInput[0] == '7')
		good = true;

	return good;
}

bool checkBoardFilledC(symbols::Board theBoard)
{

	bool filled = true;

	for (unsigned int i = 0; i < 42; i++)
	{
		if (theBoard.isEmpty(i))
		{
			filled = false;
		}
	}

	return filled;
}

bool isColumnFilledC(int col, symbols::Board theBoard)
{
	if (col == 1)
	{
		if (!theBoard.isEmpty(col0[0]))
		{
			return true;
		}
	}

	else if (col == 2)
	{
		if (!theBoard.isEmpty(col1[0]))
		{
			return true;
		}
	}

	else if (col == 3)
	{
		if (!theBoard.isEmpty(col2[0]))
		{
			return true;
		}
	}

	else if (col == 4)
	{
		if (!theBoard.isEmpty(col3[0]))
		{
			return true;
		}
	}

	else if (col == 5)
	{
		if (!theBoard.isEmpty(col4[0]))
		{
			return true;
		}
	}

	else if (col == 6)
	{
		if (!theBoard.isEmpty(col5[0]))
		{
			return true;
		}
	}

	else if (col == 7)
	{
		if (!theBoard.isEmpty(col6[0]))
		{
			return true;
		}
	}

	return false;
}


void drawClientGrid(symbols::Board* theBoard)
{
	system("cls");
	std::cout << "  1   2   3   4   5   6   7\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n";
}