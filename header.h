#pragma once

#include <iostream>
#include <vector>

#include "MessageIdentifiers.h"

#include "RakPeerInterface.h"
#include "RakNetStatistics.h"
#include "RakNetTypes.h"
#include "BitStream.h"
#include "RakSleep.h"
#include "PacketLogger.h"
#include <assert.h>
#include <cstdio>
#include <cstring>
#include <stdlib.h>
#include "Kbhit.h"
#include <stdio.h>
#include <string.h>
#include "Gets.h"
#include "GetTime.h"


namespace symbols
{

	namespace messages
	{
		enum
		{
			ID_YOUR_TURN = ID_USER_PACKET_ENUM
		};
	}

#pragma pack(push,1)
	struct Board
	{
		typedef int move_type;

		enum
		{
			EMPTY,
			CROSS,
			CIRCLE
		};

		void init()
		{

			m_ID = messages::ID_YOUR_TURN;

			for (unsigned int i = 0; i < 42; i++)
			{
				m_board[i] = EMPTY;
			}
		}

		move_type& operator[](int index){ return m_board[index]; };

		bool isEmpty(int index){ return m_board[index] == EMPTY; };

		char getSlot(int index)
		{
			if (m_board[index] == CROSS)
			{
				return 'X';
			}

			else if (m_board[index] == CIRCLE)
			{
				return 'O';
			}

			else
			{
				return '-';
			}
		}

		unsigned char m_ID;
		move_type m_board[42];
		int m_LastMovePos;
		int m_ColPos;
		int m_RowPos;


	};

	/*void doLogic(Board& b)
	{
		for (int i = 0; i < 9; i++)
		{
			if (b[i] == Board::EMPTY)
			{
				printf("%d", i);
			}

			else if (b[i] == Board::CIRCLE)
			{
				printf("%d", "O");
			}
			else if (b[i] == Board::CROSS)
			{
				printf("%d", "X");
			}
		}
	}*/
#pragma pack(pop)
}


const int arrayGap = 7;

const int col0[6] = { 0, 7, 14, 21, 28, 35 };
const int col1[6] = { 1, 8, 15, 22, 29, 36 };
const int col2[6] = { 2, 9, 16, 23, 30, 37 };
const int col3[6] = { 3, 10, 17, 24, 31, 38 };
const int col4[6] = { 4, 11, 18, 25, 32, 39 };
const int col5[6] = { 5, 12, 19, 26, 33, 40 };
const int col6[6] = { 6, 13, 20, 27, 34, 41 };

const int row0[7] = { 0, 1, 2, 3, 4, 5, 6 };
const int row1[7] = { 7, 8, 9, 10, 11, 12, 13 };
const int row2[7] = { 14, 15, 16, 17, 18, 19, 20 };
const int row3[7] = { 21, 22, 23, 24, 25, 26, 27 };
const int row4[7] = { 28, 29, 30, 31, 32, 33, 34 };
const int row5[7] = { 35, 36, 37, 38, 39, 40, 41 };


void startServer(std::vector<std::string> vec);
void startClient(std::vector<std::string> vec);

