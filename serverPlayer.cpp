#include "header.h"


#if LIBCAT_SECURITY==1
#include "SecureHandshake.h" // Include header for secure handshake
#endif

#if defined(_CONSOLE_2)
#include "Console2SampleIncludes.h"
#endif

// We copy this from Multiplayer.cpp to keep things all in one file for this example
unsigned char serverGetPacketIdentifier(RakNet::Packet *p);

#ifdef _CONSOLE_2
_CONSOLE_2_SetSystemProcessParams
#endif

void drawServerGrid(symbols::Board* theBoard);
void drawServerGrid2();

int getChipPosition(int col, symbols::Board theBoard);
bool checkInput(char theInput[2048]);
bool checkBoardFilled(symbols::Board theBoard);
bool isColumnFilled(int col, symbols::Board theBoard);
void updateBoard(symbols::Board theBoard);
bool checkConnectFourO(symbols::Board& theBoard);

void startServer(std::vector<std::string> vec)
{

	std::vector<std::string> argVector = vec;

	//start server games
#pragma region SERVER
#pragma region SERVER_START_UP
	// Pointers to the interfaces of our server and client.
	// Note we can easily have both in the same program
	RakNet::RakPeerInterface *server = RakNet::RakPeerInterface::GetInstance();
	//RakNet::RakNetStatistics *rss;
	server->SetIncomingPassword("Rumpelstiltskin", (int)strlen("Rumpelstiltskin"));
	server->SetTimeoutTime(30000, RakNet::UNASSIGNED_SYSTEM_ADDRESS);
	//	RakNet::PacketLogger packetLogger;
	//	server->AttachPlugin(&packetLogger);

#if LIBCAT_SECURITY==1
	cat::EasyHandshake handshake;
	char public_key[cat::EasyHandshake::PUBLIC_KEY_BYTES];
	char private_key[cat::EasyHandshake::PRIVATE_KEY_BYTES];
	handshake.GenerateServerKey(public_key, private_key);
	server->InitializeSecurity(public_key, private_key, false);
	FILE *fp = fopen("publicKey.dat", "wb");
	fwrite(public_key, sizeof(public_key), 1, fp);
	fclose(fp);
#endif

	// Holds packets
	RakNet::Packet* p;

	// GetPacketIdentifier returns this
	unsigned char packetIdentifier;

	// Record the first client that connects to us so we can pass it to the ping function
	RakNet::SystemAddress clientID = RakNet::UNASSIGNED_SYSTEM_ADDRESS;

	// Holds user data
	std::string portString;
	char portS[30];

	// A server
	portString = "Now listening on port (" + argVector.at(3) + ")";
	puts(portString.c_str());
	//Gets(portstring, sizeof(portstring));

	for (unsigned int i = 0; i < argVector.at(3).size(); i++)
	{
		portS[i] = argVector.at(3).at(i);
	}

	puts("Starting server.");
	// Starting the server is very simple.  2 players allowed.
	// 0 means we don't care about a connectionValidationInteger, and false
	// for low priority threads
	// I am creating two socketDesciptors, to create two sockets. One using IPV6 and the other IPV4
	RakNet::SocketDescriptor socketDescriptors[2];
	socketDescriptors[0].port = atoi(portS);
	socketDescriptors[0].socketFamily = AF_INET; // Test out IPV4
	socketDescriptors[1].port = atoi(portS);
	socketDescriptors[1].socketFamily = AF_INET6; // Test out IPV6
	bool b = server->Startup(4, socketDescriptors, 2) == RakNet::RAKNET_STARTED;
	server->SetMaximumIncomingConnections(4);
	if (!b)
	{
		printf("Failed to start dual IPV4 and IPV6 ports. Trying IPV4 only.\n");

		// Try again, but leave out IPV6
		b = server->Startup(4, socketDescriptors, 1) == RakNet::RAKNET_STARTED;
		if (!b)
		{
			puts("Server failed to start.  Terminating.");
			exit(1);
		}
	}
	server->SetOccasionalPing(true);
	server->SetUnreliableTimeout(1000);

	DataStructures::List< RakNet::RakNetSocket2* > sockets;
	server->GetSockets(sockets);
	printf("Socket addresses used by RakNet:\n");
	for (unsigned int i = 0; i < sockets.Size(); i++)
	{
		printf("%i. %s\n", i + 1, sockets[i]->GetBoundAddress().ToString(true));
	}

	printf("\nMy IP addresses:\n");
	for (unsigned int i = 0; i < server->GetNumberOfAddresses(); i++)
	{
		RakNet::SystemAddress sa = server->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, i);
		printf("%i. %s (LAN=%i)\n", i + 1, sa.ToString(false), sa.IsLANAddress());
	}

	printf("\nMy GUID is %s\n", server->GetGuidFromSystemAddress(RakNet::UNASSIGNED_SYSTEM_ADDRESS).ToString());
	//puts("'quit' to quit. 'stat' to show stats. 'ping' to ping.\n'pingip' to ping an ip address\n'ban' to ban an IP from connecting.\n'kick to kick the first connected player.\nType to talk.");
	char message[2048];
#pragma endregion

	std::cout << "Welcome to 'Connect 4'!\n";
	std::cout << "If a client is connected type 'S' to start a match\n";


	bool gameStarted = false;
	bool myTurn = false;
	bool playerConnected = false;
	bool inputGood = false;
	bool skipInput = true;
	bool colFull = false;
	//int chipPos = 0;
	symbols::Board board;
	board.init();
	// Loop for input
	while (1)
	{

		// This sleep keeps RakNet responsive
		RakSleep(30);

		if (_kbhit())
		{

	
			Gets(message, sizeof(message));

			if ((message[0] == 's' || message[0] == 'S') && playerConnected == true && gameStarted == false)
			{
				drawServerGrid2();
				gameStarted = true;
				myTurn = true;
			}

			if (gameStarted == true && myTurn == true)
			{
				inputGood = checkInput(message);

				if (inputGood)
				{
					int col = atoi(message);

					if (!isColumnFilled(col, board))
					{
						int chipPos = getChipPosition(col, board);
						board.m_board[chipPos] = symbols::Board::CROSS;
						board.m_LastMovePos = chipPos;
						
						updateBoard(board);
						checkConnectFourO(board);

						server->Send((const char *)&board, sizeof(board), HIGH_PRIORITY, RELIABLE_ORDERED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
						myTurn = false;
					}
				}
			}

			else
				std::cout << "Wait your turn!\n\n";
		}

		for (p = server->Receive(); p; server->DeallocatePacket(p), p = server->Receive())
		{
			// We got a packet, get the identifier with our handy function
			packetIdentifier = serverGetPacketIdentifier(p);

			// Check if this is a network message packet
			switch (packetIdentifier)
			{
			case ID_DISCONNECTION_NOTIFICATION:
				// Connection lost normally
				printf("ID_DISCONNECTION_NOTIFICATION from %s\n", p->systemAddress.ToString(true));;
				break;


			case ID_NEW_INCOMING_CONNECTION:
				// Somebody connected.  We have their IP now
				printf("ID_NEW_INCOMING_CONNECTION from %s with GUID %s\n", p->systemAddress.ToString(true), p->guid.ToString());
				clientID = p->systemAddress; // Record the player ID of the client

				printf("Remote internal IDs:\n");
				for (int index = 0; index < MAXIMUM_NUMBER_OF_INTERNAL_IDS; index++)
				{
					RakNet::SystemAddress internalId = server->GetInternalID(p->systemAddress, index);
					if (internalId != RakNet::UNASSIGNED_SYSTEM_ADDRESS)
					{
						printf("%i. %s\n", index + 1, internalId.ToString(true));
						playerConnected = true;
					}
				}

				break;

			case ID_INCOMPATIBLE_PROTOCOL_VERSION:
				printf("ID_INCOMPATIBLE_PROTOCOL_VERSION\n");
				break;

			case ID_CONNECTED_PING:
			case ID_UNCONNECTED_PING:
				printf("Ping from %s\n", p->systemAddress.ToString(true));
				break;

			case ID_CONNECTION_LOST:
				// Couldn't deliver a reliable packet - i.e. the other system was abnormally
				// terminated
				printf("ID_CONNECTION_LOST from %s\n", p->systemAddress.ToString(true));;
				playerConnected = false;
				break;
////////////////////////////////////////////
			case symbols::messages::ID_YOUR_TURN:
				
				using namespace symbols;
				Board* b;
				b = reinterpret_cast<Board*>(p->data);

				board = *b;

				updateBoard(board);


				if (checkConnectFourO(board))
				{
					std::cout << "GAME OVER\n\n";
				}

				else
				{
					myTurn = true;
				}

			

				break;
/////////////////////////////////////////////
			default:
				// The server knows the static data of all clients, so we can prefix the message
				// With the name data
				printf("%s\n", p->data);

				// Relay the message.  We prefix the name for other clients.  This demonstrates
				// That messages can be changed on the server before being broadcast
				// Sending is the same as before
				//sprintf(message, "%s", p->data);
				server->Send(message, (const int)strlen(message) + 1, HIGH_PRIORITY, RELIABLE_ORDERED, 0, p->systemAddress, true);

				//if (gameStarted == true)
				//{
				//	drawSGrid();
				//}

				//break;
			}

		}
	}

	server->Shutdown(300);
	// We're done with the network
	RakNet::RakPeerInterface::DestroyInstance(server);


	std::cout << "MAIN END\n";
#pragma endregion
}

	
// Copied from Multiplayer.cpp
// If the first byte is ID_TIMESTAMP, then we want the 5th byte
// Otherwise we want the 1st byte
unsigned char serverGetPacketIdentifier(RakNet::Packet *p)
{
	if (p == 0)
		return 255;

	if ((unsigned char)p->data[0] == ID_TIMESTAMP)
	{
		RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
		return (unsigned char)p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
	}
	else
		return (unsigned char)p->data[0];
}

using namespace symbols;
bool checkConnectFourO(symbols::Board& theBoard)
{
	int pos = theBoard.m_LastMovePos;
	

	//get row pos
	for (unsigned int i = 0; i < 6; i++)
	{
		//for first row
		if (i == 0)
		{
			//check elements
			for (unsigned int j = 0; j < 7; j++)
			{
				//found element in row
				if (pos == row0[j])
				{
					theBoard.m_RowPos = 0;
				}
			}
		}

		if (i == 1)
		{
			//check elements
			for (unsigned int j = 0; j < 7; j++)
			{
				//found element in row
				if (pos == row1[j])
				{
					theBoard.m_RowPos = 1;
				}
			}
		}

		if (i == 2)
		{
			//check elements
			for (unsigned int j = 0; j < 7; j++)
			{
				//found element in row
				if (pos == row2[j])
				{
					theBoard.m_RowPos = 2;
				}
			}
		}

		if (i == 3)
		{
			//check elements
			for (unsigned int j = 0; j < 7; j++)
			{
				//found element in row
				if (pos == row3[j])
				{
					theBoard.m_RowPos = 3;
				}
			}
		}

		if (i == 4)
		{
			//check elements
			for (unsigned int j = 0; j < 7; j++)
			{
				//found element in row
				if (pos == row4[j])
				{
					theBoard.m_RowPos = 4;
				}
			}
		}

		if (i == 5)
		{
			//check elements
			for (unsigned int j = 0; j < 7; j++)
			{
				//found element in row
				if (pos == row5[j])
				{
					theBoard.m_RowPos = 5;
				}
			}
		}
	}

	//get column pos
	for (unsigned int i = 0; i < 7; i++)
	{
		if (i == 0)
		{
			for (unsigned int j = 0; j < 6; j++)
			{
				if (pos == col0[j])
				{
					theBoard.m_ColPos = 0;
				}
			}
		}

		if (i == 1)
		{
			for (unsigned int j = 0; j < 6; j++)
			{
				if (pos == col1[j])
				{
					theBoard.m_ColPos = 1;
				}
			}
		}

		if (i == 2)
		{
			for (unsigned int j = 0; j < 6; j++)
			{
				if (pos == col2[j])
				{
					theBoard.m_ColPos = 2;
				}
			}
		}

		if (i == 3)
		{
			for (unsigned int j = 0; j < 6; j++)
			{
				if (pos == col3[j])
				{
					theBoard.m_ColPos = 3;
				}
			}
		}

		if (i == 4)
		{
			for (unsigned int j = 0; j < 6; j++)
			{
				if (pos == col4[j])
				{
					theBoard.m_ColPos = 4;
				}
			}
		}

		if (i == 5)
		{
			for (unsigned int j = 0; j < 6; j++)
			{
				if (pos == col5[j])
				{
					theBoard.m_ColPos = 5;
				}
			}
		}

		if (i == 6)
		{
			for (unsigned int j = 0; j < 6; j++)
			{
				if (pos == col6[j])
				{
					theBoard.m_ColPos = 6;
				}
			}
		}
	}



	int colPos = theBoard.m_ColPos;
	int rowPos = theBoard.m_RowPos;

	char board[6][7]; 

	//int holder;

	for (unsigned int i = 0; i < 6; i++)//ROW
	{
		for (unsigned int j = 0; j < 7; j++)//COL
		{
			if (i == 0)
			{
				board[i][j] = theBoard.getSlot(j);
			}

			else
			{
				board[i][j] = theBoard.getSlot((i * 7) + j);
			}
		}
	}

	int count = 0;


	for (unsigned int row = 0; row <= 2; row++)
		for (unsigned int col = 0; col <= 6; col++)
			if (board[row][col] != '-' &&
				board[row][col] == board[row + 1][col] &&
				board[row][col] == board[row + 2][col] &&
				board[row][col] == board[row + 3][col])
			{
				return true;
			}

	for (unsigned int row = 0; row <= 5; row++)
		for (unsigned int col = 0; col <= 3; col++)
			if (board[row][col] != '-' &&
				board[row][col] == board[row][col + 1] &&
				board[row][col] == board[row][col + 2] &&
				board[row][col] == board[row][col + 3])
			{
				return true;
			}


	for (unsigned int row = 0; row <= 2; row++)
		for (unsigned int col = 0; col <= 3; col++)
			if (board[row][col] != '-' &&
				board[row][col] == board[row + 1][col + 1] &&
				board[row][col] == board[row + 2][col + 2] &&
				board[row][col] == board[row + 3][col + 3])
			{
				return true;
			}
				
	for (unsigned int row = 3; row <= 5; row++)
		for (unsigned int col = 0; col <= 3; col++)
			if (board[row][col] != '-' &&
				board[row][col] == board[row - 1][col + 1] &&
				board[row][col] == board[row - 2][col + 2] &&
				board[row][col] == board[row - 3][col + 3])
			{
				return true;
			}


	return false;
}


void updateBoard(symbols::Board theBoard)
{
	system("cls");

	std::cout << "  1   2   3   4   5   6   7\n"
		<< "|-" << theBoard.getSlot(0) << "-|-" << theBoard.getSlot(1) << "-|-" << theBoard.getSlot(2) << "-|-" << theBoard.getSlot(3)
		<< "-|-" << theBoard.getSlot(4) << "-|-" << theBoard.getSlot(5) << "-|-" << theBoard.getSlot(6) << "-|\n"
		<< "|-" << theBoard.getSlot(7) << "-|-" << theBoard.getSlot(8) << "-|-" << theBoard.getSlot(9) << "-|-" << theBoard.getSlot(10)
		<< "-|-" << theBoard.getSlot(11) << "-|-" << theBoard.getSlot(12) << "-|-" << theBoard.getSlot(13) << "-|\n"
		<< "|-" << theBoard.getSlot(14) << "-|-" << theBoard.getSlot(15) << "-|-" << theBoard.getSlot(16) << "-|-" << theBoard.getSlot(17)
		<< "-|-" << theBoard.getSlot(18) << "-|-" << theBoard.getSlot(19) << "-|-" << theBoard.getSlot(20) << "-|\n"
		<< "|-" << theBoard.getSlot(21) << "-|-" << theBoard.getSlot(22) << "-|-" << theBoard.getSlot(23) << "-|-" << theBoard.getSlot(24)
		<< "-|-" << theBoard.getSlot(25) << "-|-" << theBoard.getSlot(26) << "-|-" << theBoard.getSlot(27) << "-|\n"
		<< "|-" << theBoard.getSlot(28) << "-|-" << theBoard.getSlot(29) << "-|-" << theBoard.getSlot(30) << "-|-" << theBoard.getSlot(31)
		<< "-|-" << theBoard.getSlot(32) << "-|-" << theBoard.getSlot(33) << "-|-" << theBoard.getSlot(34) << "-|\n"
		<< "|-" << theBoard.getSlot(35) << "-|-" << theBoard.getSlot(36) << "-|-" << theBoard.getSlot(37) << "-|-" << theBoard.getSlot(38)
		<< "-|-" << theBoard.getSlot(39) << "-|-" << theBoard.getSlot(40) << "-|-" << theBoard.getSlot(41) << "-|\n"; 

	bool gameOver = checkConnectFourO(theBoard);		
}

void drawServerGrid2()
{
	system("cls");

	std::cout << "  1   2   3   4   5   6   7\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n";
}


int getChipPosition(int col, symbols::Board theBoard)
{
	int chipPos = 0;

	if (col == 1)
	{
		if (theBoard.isEmpty(col0[5]))
			chipPos = col0[5];

		else if (theBoard.isEmpty(col0[4]))
			chipPos = col0[4];

		else if (theBoard.isEmpty(col0[3]))
			chipPos = col0[3];

		else if (theBoard.isEmpty(col0[2]))
			chipPos = col0[2];
		
		else if (theBoard.isEmpty(col0[1]))
			chipPos = col0[1];

		else
			chipPos = col0[0];

		return chipPos;
	}

	else if (col == 2)
	{
		if (theBoard.isEmpty(col1[5]))
			chipPos = col1[5];

		else if (theBoard.isEmpty(col1[4]))
			chipPos = col1[4];

		else if (theBoard.isEmpty(col1[3]))
			chipPos = col1[3];

		else if (theBoard.isEmpty(col1[2]))
			chipPos = col1[2];

		else if (theBoard.isEmpty(col1[1]))
			chipPos = col1[1];

		else
			chipPos = col1[0];

		return chipPos;
	}

	else if (col == 3)
	{
		if (theBoard.isEmpty(col2[5]))
			chipPos = col2[5];

		else if (theBoard.isEmpty(col2[4]))
			chipPos = col2[4];

		else if (theBoard.isEmpty(col2[3]))
			chipPos = col2[3];

		else if (theBoard.isEmpty(col2[2]))
			chipPos = col2[2];

		else if (theBoard.isEmpty(col2[1]))
			chipPos = col2[1];

		else
			chipPos = col2[0];

		return chipPos;
	}

	else if (col == 4)
	{
		if (theBoard.isEmpty(col3[5]))
			chipPos = col3[5];

		else if (theBoard.isEmpty(col3[4]))
			chipPos = col3[4];

		else if (theBoard.isEmpty(col3[3]))
			chipPos = col3[3];

		else if (theBoard.isEmpty(col3[2]))
			chipPos = col3[2];

		else if (theBoard.isEmpty(col3[1]))
			chipPos = col3[1];

		else
			chipPos = col3[0];

		return chipPos;
	}

	else if (col == 5)
	{
		if (theBoard.isEmpty(col4[5]))
			chipPos = col4[5];

		else if (theBoard.isEmpty(col4[4]))
			chipPos = col4[4];

		else if (theBoard.isEmpty(col4[3]))
			chipPos = col4[3];

		else if (theBoard.isEmpty(col4[2]))
			chipPos = col4[2];

		else if (theBoard.isEmpty(col4[1]))
			chipPos = col4[1];

		else
			chipPos = col4[0];

		return chipPos;
	}

	else if (col == 6)
	{
		if (theBoard.isEmpty(col5[5]))
			chipPos = col5[5];

		else if (theBoard.isEmpty(col5[4]))
			chipPos = col5[4];

		else if (theBoard.isEmpty(col5[3]))
			chipPos = col5[3];

		else if (theBoard.isEmpty(col5[2]))
			chipPos = col5[2];

		else if (theBoard.isEmpty(col5[1]))
			chipPos = col5[1];

		else
			chipPos = col5[0];

		return chipPos;
	}

	else if (col == 7)
	{
		if (theBoard.isEmpty(col6[5]))
			chipPos = col6[5];

		else if (theBoard.isEmpty(col6[4]))
			chipPos = col6[4];

		else if (theBoard.isEmpty(col6[3]))
			chipPos = col6[3];

		else if (theBoard.isEmpty(col6[2]))
			chipPos = col6[2];

		else if (theBoard.isEmpty(col6[1]))
			chipPos = col6[1];

		else
			chipPos = col6[0];

		return chipPos;
	}

	return 0;
}

bool checkInput(char theInput[2048])
{
	bool good = false;

	if (theInput[0] == '1')
		good = true;

	else if (theInput[0] == '2')
		good = true;

	else if (theInput[0] == '3')
		good = true;

	else if (theInput[0] == '4')
		good = true;

	else if (theInput[0] == '5')
		good = true;

	else if (theInput[0] == '6')
		good = true;

	else if (theInput[0] == '7')
		good = true;

	return good;
}

bool checkBoardFilled(symbols::Board theBoard)
{

	bool filled = true;

	for (unsigned int i = 0; i < 42; i++)
	{
		if (theBoard.isEmpty(i))
		{
			filled = false;
		}
	}

	return filled;
}

bool isColumnFilled(int col, symbols::Board theBoard)
{
	if (col == 1)
	{
		if (!theBoard.isEmpty(col0[0]))
		{
			return true;
		}
	}

	else if (col == 2)
	{
		if (!theBoard.isEmpty(col1[0]))
		{
			return true;
		}
	}

	else if (col == 3)
	{
		if (!theBoard.isEmpty(col2[0]))
		{
			return true;
		}
	}

	else if (col == 4)
	{
		if (!theBoard.isEmpty(col3[0]))
		{
			return true;
		}
	}

	else if (col == 5)
	{
		if (!theBoard.isEmpty(col4[0]))
		{
			return true;
		}
	}

	else if (col == 6)
	{
		if (!theBoard.isEmpty(col5[0]))
		{
			return true;
		}
	}

	else if (col == 7)
	{
		if (!theBoard.isEmpty(col6[0]))
		{
			return true;
		}
	}

	return false;
}

void drawServerGrid(symbols::Board* theBoard)
{
	system("cls");
	std::cout << "Enter into the console which column you'd like to drop your piece into\n\n";

	std::cout << "  1   2   3   4   5   6   7\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n"
		<< "|---|---|---|---|---|---|---|\n";
}